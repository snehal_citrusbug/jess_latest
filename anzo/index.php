<?php
// get data of input.csv

$row = 0;
if (($handle = fopen("input.csv", "r")) !== false) {
    while (($data = fgetcsv($handle, 1000000, ",")) !== false) {

        if ($row > 0) {
            //$data[0]

            $mainData[$row]['part_number'] = $data[0];
            $mainData[$row]['SKU'] = $data[0] . $data[1];
            $mainData[$row]['title'] = $data[57];
            $mainData[$row]['description'] = $data[34];
            $mainData[$row]['price'] = $data[19];
            //$image = ((isset($data[26]) && $data[26] != null) ? 'http://localhost/wordpress-project/jessSemaData/airlift/images/' . $data[26] : '');
            $mainData[$row]['image'] = '';
            $mainData[$row]['category'] = '';
            $mainData[$row]['subCategory'] = '';
            $mainData[$row]['make'] = '';
            $mainData[$row]['model'] = '';
            $mainData[$row]['year'] = '';
            $mainData[$row]['height'] = $data[36];
            $mainData[$row]['width'] = $data[37];
            $mainData[$row]['length'] = $data[38];
            $mainData[$row]['weight'] = $data[40];

        }
        $row++;

    }
    fclose($handle);
}

$makeModelYear = array();

$row = 0;
if (($handle = fopen("make-model-year.csv", "r")) !== false) {
    while (($data = fgetcsv($handle, 10000, ",")) !== false) {
        if ($row > 0) {
            if (array_key_exists($data[0], $makeModelYear)) {
                if($data[1] != null && in_array($data[1], $makeModelYear[$data[0]]['year']) == false)
                    $makeModelYear[$data[0]]['year'][] = $data[1];
                if($data[2] != null && in_array($data[2], $makeModelYear[$data[0]]['make']) == false)
                    $makeModelYear[$data[0]]['make'][] = $data[2];
                if($data[3] != null && in_array($data[3], $makeModelYear[$data[0]]['model']) == false)
                    $makeModelYear[$data[0]]['model'][] = $data[3];
            } else {
                $makeModelYear[$data[0]] = array('year' => array($data[1]) ,'make' => array($data[2]),'model' => array($data[3]));
            }
        }
        $row++;
    }
    fclose($handle);
}


foreach ($mainData as $key => $value) {
    if (array_key_exists($value['part_number'], $makeModelYear)) {
        $mainData[$key]['make'] = (isset($makeModelYear[$value['part_number']]['make']) ? implode("|",$makeModelYear[$value['part_number']]['make']) : '');
        $mainData[$key]['model'] = (isset($makeModelYear[$value['part_number']]['model']) ? implode("|",$makeModelYear[$value['part_number']]['model']) : '');
        $mainData[$key]['year'] = (isset($makeModelYear[$value['part_number']]['year']) ? implode("|", $makeModelYear[$value['part_number']]['year']) : '');
    }
}

// Put data in output.csv
$myfile = fopen("output.csv", "w");
$line = array('Part Number','SKU', 'Product Title', 'Product Long Description', 'Product Regular Price', 'Image', 'Category', 'Sub Category', 'Make', 'Model', 'Year', 'Height', 'Width', 'Length', 'Weight');
fputcsv($myfile, $line);

foreach ($mainData as $line) {
    fputcsv($myfile, $line);
}

fclose($myfile);

echo '1';